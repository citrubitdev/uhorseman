var mainCtrl = require('../controllers/mainCtrl')
module.exports = function (app) {
  app.post('/', mainCtrl.scrape)
  app.get('/toXlsl/:cacheName', mainCtrl.convertToXlsl)
  app.get('/toXlsl/:cacheName/:fileName', mainCtrl.convertToXlslWithFileName)
  app.post('/getCache', mainCtrl.getCache)
  app.post('/deleteCache', mainCtrl.deleteCache)
  app.get('/uHorsemanRegistry', mainCtrl.uHorsemanRegistry)
}
