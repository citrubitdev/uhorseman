
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var express = require('express')
var app = express()
var server = require('http').Server(app)
var json2xls = require('json2xls')
var mongoose = require('mongoose')

mongoose.connect(require('./config/database').url)

app.use(cookieParser()) // read cookies (needed for auth)
app.use(bodyParser()) // get information from html forms
app.use(bodyParser.json()) // get JSON data
app.use(json2xls.middleware)

require('./routes/mainRoute')(app)

server.listen(80, '0.0.0.0', function () {
  console.log('Listening on 80')
})
