module.exports = function (metadata, horseman, cache, done) {
  if (metadata) {
    require('./scrapeGoogle')(metadata, horseman, cache, done)
  }

  return {
    info: {
      author: 'Evans Enonchong',
      pluginName: 'scrapeGoogleResults',
      format: 'Datatype: Value: Description',
      returnDataType: 'Json Array',
      metadataType: 'Json Object'
    },

    metadata: {
      url: 'Text: Empty: The url of the website',
      searchTerm: 'Text: Empty: The search term',
      cache: 'Text: scrapeGoogleResults: The name of the data cache where the results are stored',
      plugin: 'Text: scrapeGoogleResults: The name of the plugin'
    },

    data: {
      links: 'array of links'
    }
  }
}
