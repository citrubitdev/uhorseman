module.exports = function (metadata, horseman, cache, done) {
  var url = metadata.url
  var searchTerm = metadata.searchTerm
  var cacheName = metadata.cache
  horseman
  .open('https://google.com')
  .html()
  .evaluate(function (selector) {
    var dataset = []
    $('a').each(function () {
      var data = {}
      data.link = $(this)
      dataset.push(data)
    })
    return dataset
  })
  .then(function (dataset) {
    cache.cachePushData(cacheName, dataset, function (err) {
      if (!err) {
        done({ complete: true, err: '', metadata: metadata })
      } else {
        done({ complete: false, error: err, metadata: metadata })
      }
    })
    horseman.close()
  })
}
