
var uHorseman = require('../uHorseman')('./uHorsemanConfig.json')

var cache = uHorseman.crawlerCache

module.exports.scrape = function (req, res) {
  var metadata = req.body.metadata
  console.log(metadata)
  uHorseman.horsemanReq(metadata, function (response) {
    if (response) {
      res.send(JSON.stringify(response))
    } else {
      res.status(404).end()
    }
  })
}


module.exports.scrapeAsync = function (req, res) {
  var metadataArr = req.body.metadataArr
  uHorseman.horsemanAsync(metadataArr, function (err, response) {
    if (!err) {
      res.send(JSON.stringify(response))
    }
  })
}


module.exports.getCache = function (req, res) {
  var cacheName = req.body.cacheName
  cache.getCache(cacheName, function (err, cacheData) {
    if (!err) {
      res.send(JSON.stringify(cacheData))
    } else {
      res.status(404).end()
    }
  })
}


module.exports.deleteCache = function (req, res) {
  var cacheName = req.body.cacheName
  cache.deleteCache(cacheName, function (err) {
    if (!err) {
      res.status(200).end()
    } else {
      res.status(404).end()
    }
  })
}


module.exports.convertToXlsl = function (req, res) {
  var cacheName = req.params.cacheName
  cache.getCache(cacheName, function (err, cacheData) {
    if (!err) {
      res.xls('data.xlsl', cacheData)
    } else {
      res.status(404).end()
    }
  })
}


module.exports.convertToXlslWithFileName = function (req, res) {
  var cacheName = req.params.cacheName
  var fileName = req.params.fileName
  var fullFileName = fileName + '.xlsx'
  cache.getCache(cacheName, function (err, cacheData) {
    if (!err) {
      res.xls(fullFileName, cacheData)
    } else {
      res.status(404).end()
    }
  })
}


module.exports.uHorsemanRegistry = function (req, res) {
  res.send(uHorseman.showInstalledPlugins())
}
