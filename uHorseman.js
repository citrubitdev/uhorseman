var mongoose = require('mongoose')
var Horseman = require('node-horseman')
var jsonfile = require('jsonfile')
var horsemanPluginCtrl = require('./controllers/uHorsemanPluginCtrl')
var registry = require('./uHorsemanRegistry').registry
var pluginsInfo = []



var randomUserAgent = function (userAgentArray) {
  return userAgentArray[Math.floor((Math.random() * userAgentArray.length) + 0)]
}


var Schema = mongoose.Schema
var CacheSchema = mongoose.Schema({
  name: {type: String},
  store: [Schema.Types.Mixed]
})

var Cache = mongoose.model('cache', CacheSchema)

module.exports = function (configLocation) {
  var config = jsonfile.readFileSync(configLocation)
  var cache = { cachePushData: function (cacheName, dataset, cb) {
    Cache.findOne({ name: cacheName }, function (err, cache) {
      if (!err && cache) {
        dataset.forEach(function (data) {
          cache.store.push(data)
        })
        cache.save(function (err, cache) {
          if (!err) {
            cb(null, cache.store)
          } else {
            cb(err)
          }
        })
      } else {
        var newCache = new Cache()
        newCache.name = cacheName
        dataset.forEach(function (data) {
          newCache.store.push(data)
        })
        newCache.save(function (err, cache) {
          if (!err) {
            cb(null, cache.store)
          } else {
            cb(err)
          }
        })
      }
    })
  },
    deleteCache: function (cacheName, cb) {
      Cache.findOneAndRemove({ name: cacheName }, function (err) {
        if (!err) {
          cb(null)
        } else {
          cb(err)
        }
      })
    },
    getCache: function (cacheName, cb) {
      Cache.findOne({ name: cacheName }, function (err, cache) {
        if (!err && cache) {
          cb(null, cache.store)
        } else {
          cb(err)
        }
      })
    } }


  return {
    plugins: pluginsInfo,
    crawlerCache: cache,
    horsemanReq: function (metadata, cb) {
      metadata = JSON.parse(metadata)
      var responseStack = []
      function done (response) {
        responseStack.push({ response: response.complete, error: '', metadata: response.metadata })
        cb(responseStack)
      }
      console.log(config.horseman.options)
      var horseman = new Horseman(config.horseman.options)
      .userAgent(randomUserAgent(config.userAgentArr))
      .on('resourceError', function (resourceError) {
        responseStack.push({ response: false, error: resourceError, metadata: metadata })
      })
      var plugin = horsemanPluginCtrl[metadata.plugin]
      console.log(plugin)
      if (typeof plugin === 'function') {
        setTimeout(function () {
          plugin(metadata, horseman, cache, done)
        }, 0)
      } else {
        responseStack.push({ response: false, error: 'plugin not installed', metadata: metadata })
        cb(responseStack)
      }
    },

    horsemanAsync: function (metadataArr, cb) {
      var responseStack = []
      function done (response) {
        responseStack.push({ response: response.complete, error: '', metadata: response.metadata })
        if (metadataArr.length === responseStack.length) {
          cb(responseStack)
        }
      }

      metadataArr.forEach(function (metadata) {
        var horseman = new Horseman(config.horseman.options)
        .userAgent(randomUserAgent(config.userAgentArr))
        .on('resourceError', function (resourceError) {
          responseStack.push({ response: false, error: resourceError, metadata: metadata })
        })
        var plugin = horsemanPluginCtrl[metadata.plugin]
        if (typeof plugin === 'function') {
          setTimeout(function () {
            plugin(metadata, horseman, cache, done)
          }, 0)
        } else {
          responseStack.push({ response: false, error: 'plugin not installed', metadata: metadata })
        }
      })
    },
    showInstalledPlugins: function () {
      return registry
    }
  }
}













